<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 *
 * @Route("/api", name="api_")
 *
 * @package App\Controller
 */
class DefaultController extends AbstractController
{

    /**
     * @Route("/login", name="login")
     */
    public function login()
    {
       return new JsonResponse("LOGIN ROUTE", JsonResponse::HTTP_OK);
    }

}
