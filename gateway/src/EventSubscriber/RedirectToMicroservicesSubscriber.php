<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;


class RedirectToMicroservicesSubscriber implements EventSubscriberInterface
{
    public function urlRedirectAction(Request $request, string $scheme, int $httpPort, $host): RedirectResponse
    {

        $url = $scheme . '://' . $host . ':' . $httpPort . $request->getRequestUri();

        return new RedirectResponse($url, 308);
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();

        $uri = $request->getRequestUri();

        $response = null;

        if(strpos($uri, '/api/log/') !== false)
            $response = $this->urlRedirectAction($request, 'http', '8001', 'localhost');
        else if(strpos($uri, '/api/afiliado/') !== false)
            $response = $this->urlRedirectAction($request, 'http', '8002', 'localhost');
        else if(strpos($uri, '/api/equipos/') !== false)
            $response = $this->urlRedirectAction($request, 'http', '8003', 'localhost');
        else if(strpos($uri, '/api/user/') !== false)
            $response = $this->urlRedirectAction($request, 'http', '8004', 'localhost');

        if($response)
            $event->setResponse($response);
    }

    public static function getSubscribedEvents()
    {
        return [
            RequestEvent::class => 'onKernelRequest',
        ];
    }
}
