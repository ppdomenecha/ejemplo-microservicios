<?php

namespace App\Controller;

use App\Entity\Log;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LogController
 *
 * @Route("/api/log", name="api_log_")
 *
 * @package App\Controller
 */
class LogController extends AbstractController
{
    /**
     * @Route("/create", name="log", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function addLog(Request $request)
    {
        $actor = $request->get('actor');
        $action = $request->get('action');
        $context = $request->get('context');

        if(!$actor || !$action || !$context){
            return new JsonResponse("Required params: actor, action, context", JsonResponse::HTTP_BAD_REQUEST);
        }
        else return new JsonResponse("Todos los parametros son correctos");

        $log = new Log();
        $log->setAction($action);
        $log->setActor($actor);
        $log->setContext($context);

    }

    /**
     * @Route("/queue", name="queue", methods={"POST"})
     */
    public function enqueueMessage()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/LogController.php',
        ]);
    }

    /**
     * @Route("/list", name="list", methods={"GET"})
     */
    public function listLogs()
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine');
        $logs = $em->getRepository(Log::class)->findAll();
        dd($logs);
    }
}
